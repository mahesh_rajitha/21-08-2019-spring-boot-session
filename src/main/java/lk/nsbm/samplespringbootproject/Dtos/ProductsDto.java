package lk.nsbm.samplespringbootproject.Dtos;

import lombok.Data;

@Data
public class ProductsDto {

    private String id;
    private String category;
    private String productName;
}
