package lk.nsbm.samplespringbootproject.services;

import lk.nsbm.samplespringbootproject.Dtos.ProductsDto;

import java.util.List;

public interface ProductServices {
    ProductsDto saveProduct(ProductsDto productsDto);
    List<ProductsDto> getProducts();
}
