package lk.nsbm.samplespringbootproject.services.servicesimpl;

import lk.nsbm.samplespringbootproject.Dtos.ProductsDto;
import lk.nsbm.samplespringbootproject.models.ProductModel;
import lk.nsbm.samplespringbootproject.repositories.ProductRepository;
import lk.nsbm.samplespringbootproject.services.ProductServices;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

@Service
@Slf4j
public class ProductServicesImpl implements ProductServices {
    @Autowired
    private ProductRepository productRepository;

    @Override
    public ProductsDto saveProduct(ProductsDto productsDto){
        ProductModel productModel = new ProductModel();
        productModel.setId(UUID.randomUUID().toString());
        productModel.setCategory(productsDto.getCategory());
        productModel.setProductName(productsDto.getProductName());
        log.info(productModel.getId());
        log.info("",productRepository.save(productModel));
        return productsDto;
    }

    @Override
    public List<ProductsDto> getProducts(){
        return productRepository.findAll().stream().map(this::toDto).collect(Collectors.toList());
    }

    private ProductsDto toDto(ProductModel productModel){
        ProductsDto productsDto = new ProductsDto();
        productsDto.setProductName(productModel.getProductName());
        productsDto.setId(productModel.getId());
        productsDto.setCategory(productModel.getCategory());
        return productsDto;
    }
}
