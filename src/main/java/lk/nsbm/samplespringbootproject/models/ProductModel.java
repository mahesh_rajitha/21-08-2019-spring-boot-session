package lk.nsbm.samplespringbootproject.models;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity
public class ProductModel {
    @Id
    private String id;
    private String category;
    private String productName;
}
