package lk.nsbm.samplespringbootproject.repositories;

import lk.nsbm.samplespringbootproject.models.ProductModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<ProductModel,String> {

}
