package lk.nsbm.samplespringbootproject.controllers;

import lk.nsbm.samplespringbootproject.Dtos.ProductsDto;
import lk.nsbm.samplespringbootproject.services.ProductServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/product")
public class ProductController {
    @Autowired
    private ProductServices productServices;
    @PostMapping("add")
    public ResponseEntity<?> saveProduct(@RequestBody ProductsDto productsDto){
        return ResponseEntity.status(HttpStatus.OK).body(productServices.saveProduct(productsDto));
    }
    @GetMapping("all-products")
    public ResponseEntity<?> getAllProducts(){
        return ResponseEntity.status(HttpStatus.OK).body(productServices.getProducts());
    }
}
